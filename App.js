import { useState } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { advices } from './apis/advices';
export default function App() {

  const [advice, setAdvice] = useState("")

  const getAdvice = () => {
    advices.getAdvice()
      .then((data) => {
        setAdvice(data.slip.advice);
      });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.advice}>{advice}</Text>
      <Button title="Get Advice"
        onPress={getAdvice} color="green" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 10
  },
});
