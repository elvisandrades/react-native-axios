import axios from 'axios';

const BASE_URL = "http://api.adviceslip.com/";

export const client = axios.create({
    baseURL: BASE_URL
});