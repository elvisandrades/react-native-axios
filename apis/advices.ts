import { client } from "./ClientAxios";

const endpoint = "advice/";

export const advices = {
    getAdvice : async function(){
        const url = endpoint + 
        getRandomId(1, 50);

         const response =  await client.get(url);

         return response.data;
    }
}

const getRandomId = (min: number, max: number) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return (Math.floor(Math.random() * 
        (max - min + 1)) + min).toString();
};